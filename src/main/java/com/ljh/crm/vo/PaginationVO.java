package com.ljh.crm.vo;

import java.util.List;

public class PaginationVO<T> {
    private int total ;//总页数
    private List<T> dataList;//返回一组列表

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<T> getDataList() {
        return dataList;
    }

    public void setDataList(List<T> dataList) {
        this.dataList = dataList;
    }
}
