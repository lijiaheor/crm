package com.ljh.crm.web.listener;

import com.ljh.crm.settings.pojo.DicValue;
import com.ljh.crm.settings.service.DicService;
import com.ljh.crm.settings.service.impl.DicServiceImpl;
import com.ljh.crm.utils.ServiceFactory;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 用来监听上下文本域对象的方法,当服务器启动,上下文域对象创建对象创建完毕后,马上执行该方法
 */
public class SysInitListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent event) {
        System.out.println("服务器缓存处理数据字典");
        ServletContext application = event.getServletContext();
        DicService ds = (DicService) ServiceFactory.getService(new DicServiceImpl());
        //取数据字典
        Map<String, List<DicValue>> map = ds.getAll();
        Set<String> set = map.keySet();
        for (String key : set){
            application.setAttribute(key,map.get(key));
        }

    }
}
