package com.ljh.crm.settings.service.impl;

import com.ljh.crm.exception.LoginException;
import com.ljh.crm.settings.mapper.UserMapper;
import com.ljh.crm.settings.pojo.User;
import com.ljh.crm.settings.service.UserService;
import com.ljh.crm.utils.DateTimeUtil;
import com.ljh.crm.utils.SqlSessionUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class UserServiceImpl implements UserService {

    private UserMapper userDao = SqlSessionUtil.getSqlSession().getMapper(UserMapper.class);


    public User login(String loginAct, String loginPwd, String ip) throws LoginException {
        Map<String, String> map = new HashMap<String, String>();
        map.put("loginAct", loginAct);
        map.put("loginPwd", loginPwd);
        User user = userDao.login(map);

        if (user == null) {
            throw new LoginException("账号密码错误");
        }
        //如果程序能够成功执行到该行,说明账户密码正确

        //验证失效时间
        String expireTime = user.getExpireTime();
        String currentTime = DateTimeUtil.getSysTime();
        if (expireTime.compareTo(currentTime) < 0) {
            throw new LoginException("账号已经失效");
        }

        //判断锁定状态
        String lockState = user.getLockState();
        if ("0".equals(lockState)) {
            throw new LoginException("账号已锁定");
        }

        //判断ip地址
        String allowIps = user.getAllowIps();
//        if(allowIps != null && ""){
//
//        }
        if (!allowIps.contains(ip)) {
            throw new LoginException("ip地址受限,请联系管理员");
        }
        return user;
    }

    public List<User> getUserList() {
        List<User> list = userDao.getUserList();
        return list;
    }
}




































