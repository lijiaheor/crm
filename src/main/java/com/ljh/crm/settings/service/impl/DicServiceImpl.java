package com.ljh.crm.settings.service.impl;

import com.ljh.crm.settings.mapper.DicTypeMapper;
import com.ljh.crm.settings.mapper.DicValueMapper;
import com.ljh.crm.settings.pojo.DicType;
import com.ljh.crm.settings.pojo.DicValue;
import com.ljh.crm.settings.service.DicService;
import com.ljh.crm.utils.SqlSessionUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DicServiceImpl implements DicService {
    private DicTypeMapper dicTypeMapper = SqlSessionUtil.getSqlSession().getMapper(DicTypeMapper.class);
    private DicValueMapper dicValueMapper = SqlSessionUtil.getSqlSession().getMapper(DicValueMapper.class);
    @Override
    public Map<String, List<DicValue>> getAll() {
        Map<String, List<DicValue>> map = new HashMap<>();
        //将字典类型列表取出
        List<DicType> dtList = dicTypeMapper.getTypeList();
        //将字典类型列表遍历出来
        for(DicType dt : dtList){
            //获取到每个字典类型的编码
            String code = dt.getCode();
            //根据每个字典类型来取得字典值列表
            List<DicValue> dvList = dicValueMapper.getListByCode(code);
            map.put(code + "List",dvList);
        }
        return map;
    }
}
