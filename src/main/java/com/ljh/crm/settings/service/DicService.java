package com.ljh.crm.settings.service;

import com.ljh.crm.settings.pojo.DicValue;

import java.util.List;
import java.util.Map;

public interface DicService {
    //获取数据字典值的列表
    Map<String, List<DicValue>> getAll();
}
