package com.ljh.crm.settings.service;


import com.ljh.crm.exception.LoginException;
import com.ljh.crm.settings.pojo.User;

import java.util.List;

public interface UserService {
    /**
     * 验证登录操作
     *
     * @param loginAct 登录账号
     * @param loginPwd 登录密码
     * @param ip       当前ip
     * @return
     * @throws LoginException
     */
    User login(String loginAct, String loginPwd, String ip) throws LoginException;
    //  执行市场添加操作的模态窗口(获取用户列表信息)
    List<User> getUserList();
}
