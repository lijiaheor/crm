package com.ljh.crm.settings.mapper;

import com.ljh.crm.settings.pojo.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;
@Mapper
public interface UserMapper {
    //验证登录
    User login(Map<String, String> map);
    //获取用户列表
    List<User> getUserList();
}
