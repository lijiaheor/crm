package com.ljh.crm.settings.mapper;

import com.ljh.crm.settings.pojo.DicType;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
@Mapper
public  interface DicTypeMapper {
    //获取字典类型列表
     List<DicType> getTypeList() ;
}
