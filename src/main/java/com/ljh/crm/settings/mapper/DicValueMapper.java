package com.ljh.crm.settings.mapper;

import com.ljh.crm.settings.pojo.DicValue;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface DicValueMapper {
    //根据字典类型(code)来获取字典值列表并且按照orderNo默认降序排序出来
    List<DicValue> getListByCode(String code);
}
