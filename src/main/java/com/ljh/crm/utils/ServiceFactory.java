package com.ljh.crm.utils;

/**
 * service代理工具
 */
public class ServiceFactory {

    public static Object getService(Object service) {

        return new TransactionInvocationHandler(service).getProxy();

    }

}
