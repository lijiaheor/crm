package com.ljh.crm.workbench.web.controller;

import com.ljh.crm.settings.pojo.User;
import com.ljh.crm.settings.service.UserService;
import com.ljh.crm.settings.service.impl.UserServiceImpl;
import com.ljh.crm.utils.DateTimeUtil;
import com.ljh.crm.utils.PrintJson;
import com.ljh.crm.utils.ServiceFactory;
import com.ljh.crm.utils.UUIDUtil;
import com.ljh.crm.vo.PaginationVO;
import com.ljh.crm.workbench.pojo.Activity;
import com.ljh.crm.workbench.pojo.ActivityRemark;
import com.ljh.crm.workbench.service.ActivityService;
import com.ljh.crm.workbench.service.TranService;
import com.ljh.crm.workbench.service.impl.ActivityServiceImpl;
import com.ljh.crm.workbench.service.impl.TranServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ActivityController extends HttpServlet {
    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("进入到市场活动控制器");
        String path = request.getServletPath();
        if ("/workbench/activity/getUserList.do".equals(path)) {
            getUserList(request, response);
        } else if ("/workbench/activity/save.do".equals(path)) {
            save(request, response);
        } else if ("/workbench/activity/pageList.do".equals(path)) {
            pageList(request, response);
        } else if ("/workbench/activity/deletebatch.do".equals(path)) {
            delete(request, response);
        } else if ("/workbench/activity/getUserListAndActivity.do".equals(path)) {
            getUserListAndActivity(request, response);
        } else if ("/workbench/activity/update.do".equals(path)) {
            update(request, response);
        } else if ("/workbench/activity/detail.do".equals(path)) {
            detail(request, response);
        } else if ("/workbench/activity/getRemarkListByAid.do".equals(path)) {
            getRemarkListByAid(request, response);
        } else if ("/workbench/activity/deleteRemark.do".equals(path)) {
            deleteRemark(request, response);
        } else if ("/workbench/activity/saveRemark.do".equals(path)) {
            saveRemark(request, response);
        } else if ("/workbench/activity/updateRemark.do".equals(path)) {
            updateRemark(request, response);
        } else if ("/workbench/activity/getCharts.do".equals(path)) {
            getCharts(request, response);
        }
    }

    //    为市场活动的名称和成本制作柱状图
    private void getCharts(HttpServletRequest request, HttpServletResponse response) {
        System.out.println("进入到绘制市场活动柱状图的操作");
        ActivityService activityService = (ActivityService) ServiceFactory.getService(new ActivityServiceImpl());
        Map<String,Object> map = activityService.getCharts();
        PrintJson.printJsonObj(response,map);
    }

    //  为进入到市场活动详细页面后对备注进行修改操作
    private void updateRemark(HttpServletRequest request, HttpServletResponse response) {
        System.out.println("进入到对备注进行修改的操作");

        String id = request.getParameter("id");
        String noteContent = request.getParameter("noteContent");
        String editTime = DateTimeUtil.getSysTime();
        String editBy = ((User) request.getSession().getAttribute("user")).getName();

        ActivityRemark ar = new ActivityRemark();
        ar.setId(id);
        ar.setNoteContent(noteContent);
        ar.setEditTime(editTime);
        ar.setEditBy(editBy);
        ActivityService activityService = (ActivityService) ServiceFactory.getService(new ActivityServiceImpl());

        Boolean flag = activityService.updateRemark(ar);
        Map<String, Object> map = new HashMap<>();
        map.put("success", flag);
        map.put("ar", ar);
        PrintJson.printJsonObj(response, map);

    }

    // 为进入到市场活动详细页面后对备注进行添加操作
    private void saveRemark(HttpServletRequest request, HttpServletResponse response) {
        System.out.println("进入到添加市场备注的操作");
        String id = UUIDUtil.getUUID();
        String activityId = request.getParameter("activityId");
        String noteContent = request.getParameter("noteContent");
        String createTime = DateTimeUtil.getSysTime();
        String createBy = ((User) request.getSession().getAttribute("user")).getName();
        String editFlag = "0";
        ActivityRemark ar = new ActivityRemark();
        ar.setId(id);
        ar.setActivityId(activityId);
        ar.setCreateTime(createTime);
        ar.setCreateBy(createBy);
        ar.setNoteContent(noteContent);
        ar.setEditFlag(editFlag);

        ActivityService activityService = (ActivityService) ServiceFactory.getService(new ActivityServiceImpl());
        Boolean flag = activityService.saveRemark(ar);

        Map<String, Object> map = new HashMap<>();
        map.put("success", flag);
        map.put("ar", ar);
        PrintJson.printJsonObj(response, map);
    }

    //为进入到市场活动详细页面后对备注进行删除操作
    private void deleteRemark(HttpServletRequest request, HttpServletResponse response) {
        System.out.println("进入到根据id删除市场备注信息");
        String id = request.getParameter("id");
        ActivityService activityService = (ActivityService) ServiceFactory.getService(new ActivityServiceImpl());
        boolean flag = activityService.deleteRemark(id);
        PrintJson.printJsonFlag(response, flag);
    }

    //进入到市场活动页面后进行加载备注信息列表
    private void getRemarkListByAid(HttpServletRequest request, HttpServletResponse response) {
        System.out.println("进入到根据市场活动id,取得备注信息列表");

        String activityId = request.getParameter("activityId");
        System.out.println(activityId);

        ActivityService activityService = (ActivityService) ServiceFactory.getService(new ActivityServiceImpl());

        List<ActivityRemark> activityRemarkList = activityService.getRemarkListByAid(activityId);

        PrintJson.printJsonObj(response, activityRemarkList);
    }

    //市场活动信息跳转到详细信息页面
    private void detail(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("进入到市场活动跳转到详细信息页面的操作");
        String id = request.getParameter("id");

        ActivityService activityService = (ActivityService) ServiceFactory.getService(new ActivityServiceImpl());
        Activity a = activityService.detail(id);
        request.setAttribute("a", a);
        request.getRequestDispatcher("/workbench/activity/detail.jsp").forward(request, response);

    }

    //为修改模态窗口的更新按钮绑定事件执行信息的修改
    private void update(HttpServletRequest request, HttpServletResponse response) {
        System.out.println("进入到根据用户id修改用户的操作");

        String id = request.getParameter("id");
        String owner = request.getParameter("owner");
        String name = request.getParameter("name");
        String startDate = request.getParameter("startDate");
        String endDate = request.getParameter("endDate");
        String cost = request.getParameter("cost");
        String description = request.getParameter("description");
        //修改时间为当前系统时间
        String editTime = DateTimeUtil.getSysTime();
        //修改人:当前登录用户
        String editBy = ((User) request.getSession().getAttribute("user")).getName();

        Activity a = new Activity();
        a.setId(id);
        a.setOwner(owner);
        a.setName(name);
        a.setStartDate(startDate);
        a.setEndDate(endDate);
        a.setCost(cost);
        a.setDescription(description);
        a.setEditTime(editTime);
        a.setEditBy(editBy);

        ActivityService activityService = (ActivityService) ServiceFactory.getService(new ActivityServiceImpl());

        boolean flag = activityService.update(a);

        PrintJson.printJsonFlag(response, flag);
    }

    //打开修改模态窗口时显示的信息(查询用户信息列表和根据市场活动id查询单条记录的操作)
    private void getUserListAndActivity(HttpServletRequest request, HttpServletResponse response) {
        System.out.println("进入到查询用户信息列表和根据市场活动id查询单条记录的操作");
        String id = request.getParameter("id");
        ActivityService activityService = (ActivityService) ServiceFactory.getService(new ActivityServiceImpl());
        Map<String, Object> map = activityService.getUserListAndActivity(id);
        PrintJson.printJsonObj(response, map);
    }

    //进行删除操作
    private void delete(HttpServletRequest request, HttpServletResponse response) {
        System.out.println("进入到市场活动的删除操作");

        String[] ids = request.getParameterValues("id");

        ActivityService activityService = (ActivityService) ServiceFactory.getService(new ActivityServiceImpl());

        boolean flag = activityService.delete(ids);
        PrintJson.printJsonFlag(response, flag);
    }

    //   分页查询 加条件查询
    private void pageList(HttpServletRequest request, HttpServletResponse response) {
        System.out.println("进入到查询市场活动信息列表的操作(条件查询和分页查询)");

        String name = request.getParameter("name");
        String owner = request.getParameter("owner");
        String startDate = request.getParameter("startDate");
        String endDate = request.getParameter("endDate");
        //显示的是那一页
        String pageNoStr = request.getParameter("pageNo");
        int pageNo = Integer.parseInt(pageNoStr);
        //每页展现的记录数
        String pageSizeStr = request.getParameter("pageSize");
        int pageSize = Integer.parseInt(pageSizeStr);

        //计算出略过的记录数
        int skipCount = (pageNo - 1) * pageSize;

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("name", name);
        map.put("owner", owner);
        map.put("startDate", startDate);
        map.put("endDate", endDate);
        map.put("skipCount", skipCount);
        map.put("pageSize", pageSize);

        ActivityService activityService = (ActivityService) ServiceFactory.getService(new ActivityServiceImpl());
        //设置一个vo类方便用来返回给前端的值,使用泛型可以使得这个类可以多次使用。
        PaginationVO<Activity> vo = activityService.pageList(map);

        PrintJson.printJsonObj(response, vo);
    }

    //   为执行市场保存按钮执行添加操作
    private void save(HttpServletRequest request, HttpServletResponse response) {
        System.out.println("执行市场活动添加操作");


        String owner = request.getParameter("owner");
        String name = request.getParameter("name");
        String startDate = request.getParameter("startDate");
        String endDate = request.getParameter("endDate");
        String cost = request.getParameter("cost");
        String description = request.getParameter("description");
        //创建时间为当前系统时间
        String createTime = DateTimeUtil.getSysTime();
        //创建人:当前登录用户
        String createBy = ((User) request.getSession().getAttribute("user")).getName();

        Activity a = new Activity();
        a.setId(UUIDUtil.getUUID());
        a.setOwner(owner);
        a.setName(name);
        a.setStartDate(startDate);
        a.setEndDate(endDate);
        a.setCost(cost);
        a.setDescription(description);
        a.setCreateTime(createTime);
        a.setCreateBy(createBy);

        ActivityService activityService = (ActivityService) ServiceFactory.getService(new ActivityServiceImpl());

        boolean flag = activityService.save(a);

        PrintJson.printJsonFlag(response, flag);
    }

    //  执行市场添加操作的模态窗口(获取用户列表信息)
    private void getUserList(HttpServletRequest request, HttpServletResponse response) {
        System.out.println("获取用户信息列表");

        UserService us = (UserService) ServiceFactory.getService(new UserServiceImpl());

        List<User> list = us.getUserList();

        PrintJson.printJsonObj(response, list);
    }

}
