package com.ljh.crm.workbench.web.controller;

import com.ljh.crm.settings.pojo.User;
import com.ljh.crm.utils.DateTimeUtil;
import com.ljh.crm.utils.PrintJson;
import com.ljh.crm.utils.ServiceFactory;
import com.ljh.crm.utils.UUIDUtil;
import com.ljh.crm.vo.PaginationVO;
import com.ljh.crm.workbench.pojo.Contacts;
import com.ljh.crm.workbench.pojo.ContactsRemark;
import com.ljh.crm.workbench.service.ContactsService;
import com.ljh.crm.workbench.service.impl.ContactsServiceimpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ContactsController extends HttpServlet {
    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("进入交易控制器");
        String path = request.getServletPath();
        if ("/workbench/contacts/pageList.do".equals(path)) {
            pageList(request, response);
        } else if ("/workbench/contacts/detail.do".equals(path)) {
            detail(request, response);
        } else if ("/workbench/contacts/getRemarkListByCid.do".equals(path)) {
            getRemarkListByCid(request, response);
        } else if ("/workbench/contacts/saveRemark.do".equals(path)) {
            saveRemark(request, response);
        } else if ("/workbench/contacts/save.do".equals(path)) {
            save(request, response);
        } else if ("/workbench/contacts/getUserListAndContacts.do".equals(path)) {
            getUserListAndContacts(request, response);
        } else if ("/workbench/contacts/update.do".equals(path)) {
            update(request, response);
        } else if ("/workbench/contacts/delete.do".equals(path)) {
            delete(request, response);
        }
    }

    //    进行联系人的删除操作
    private void delete(HttpServletRequest request, HttpServletResponse response) {
        System.out.println("执行删除联系人的操作");

        String ids[] = request.getParameterValues("id");
        ContactsService contactsService = (ContactsService) ServiceFactory.getService(new ContactsServiceimpl());
        boolean flag = contactsService.delete(ids);
        PrintJson.printJsonFlag(response, flag);
    }

    //    进行联系人信息的修改
    private void update(HttpServletRequest request, HttpServletResponse response) {
        System.out.println("进入到根据用户id修改客户信息的操作");
        String id = request.getParameter("id");
        String owner = request.getParameter("owner");
        String source = request.getParameter("source");
        String fullname = request.getParameter("fullname");
        String appellation = request.getParameter("appellation");
        String email = request.getParameter("email");
        String mphone = request.getParameter("mphone");
        String job = request.getParameter("job");
        String birth = request.getParameter("birth");
        String contactSummary = request.getParameter("contactSummary");
        String nextContactTime = request.getParameter("nextContactTime");
        String description = request.getParameter("description");
        String address = request.getParameter("address");
        String editBy = ((User) request.getSession().getAttribute("user")).getName();
        String editTime = DateTimeUtil.getSysTime();

        Contacts a = new Contacts();
        a.setId(id);
        a.setFullname(fullname);
        a.setOwner(owner);
        a.setSource(source);
        a.setAppellation(appellation);
        a.setEmail(email);
        a.setMphone(mphone);
        a.setJob(job);
        a.setBirth(birth);
        a.setEditBy(editBy);
        a.setEditTime(editTime);
        a.setDescription(description);
        a.setContactSummary(contactSummary);
        a.setNextContactTime(nextContactTime);
        a.setAddress(address);

        ContactsService contactsService = (ContactsService) ServiceFactory.getService(new ContactsServiceimpl());

        boolean flag = contactsService.update(a);

        PrintJson.printJsonFlag(response, flag);
    }

    //    为修改按钮绑定事件，打开修改操作的模态窗口
    private void getUserListAndContacts(HttpServletRequest request, HttpServletResponse response) {
        System.out.println("进入到查询用户信息列表和根据线索id查询单条联系人记录的操作");
        String id = request.getParameter("id");
        ContactsService contactsService = (ContactsService) ServiceFactory.getService(new ContactsServiceimpl());
        Map<String, Object> map = contactsService.getUserListAndCustomer(id);
        PrintJson.printJsonObj(response, map);
    }

    //    进行联系人的添加操作
    private void save(HttpServletRequest request, HttpServletResponse response) {
        System.out.println("执行联系人的添加操作");

        String owner = request.getParameter("owner");
        String source = request.getParameter("source");
        String fullname = request.getParameter("fullname");
        String appellation = request.getParameter("appellation");
        String email = request.getParameter("email");
        String mphone = request.getParameter("mphone");
        String job = request.getParameter("job");
        String birth = request.getParameter("birth");
        String description = request.getParameter("description");
        String contactSummary = request.getParameter("contactSummary");
        String nextContactTime = request.getParameter("nextContactTime");
        String address = request.getParameter("address");
        String createBy = ((User) request.getSession().getAttribute("user")).getName();
        String createTime = DateTimeUtil.getSysTime();

        Contacts c = new Contacts();
        c.setId(UUIDUtil.getUUID());
        c.setSource(source);
        c.setFullname(fullname);
        c.setAppellation(appellation);
        c.setEmail(email);
        c.setMphone(mphone);
        c.setJob(job);
        c.setBirth(birth);
        c.setOwner(owner);
        c.setCreateBy(createBy);
        c.setDescription(description);
        c.setCreateTime(createTime);
        c.setContactSummary(contactSummary);
        c.setNextContactTime(nextContactTime);
        c.setAddress(address);

        ContactsService contactsService = (ContactsService) ServiceFactory.getService(new ContactsServiceimpl());
        boolean flag = contactsService.save(c);
        PrintJson.printJsonFlag(response, flag);
    }

    //    对联系人备注信息进行添加
    private void saveRemark(HttpServletRequest request, HttpServletResponse response) {
        System.out.println("进入到添加联系人备注的操作");
        String id = UUIDUtil.getUUID();
        String contactsId = request.getParameter("contactsId");
        String noteContent = request.getParameter("noteContent");
        String createTime = DateTimeUtil.getSysTime();
        String createBy = ((User) request.getSession().getAttribute("user")).getName();
        String editFlag = "0";
        ContactsRemark ar = new ContactsRemark();
        ar.setId(id);
        ar.setContactsId(contactsId);
        ar.setCreateTime(createTime);
        ar.setCreateBy(createBy);
        ar.setNoteContent(noteContent);
        ar.setEditFlag(editFlag);

        ContactsService contactsService = (ContactsService) ServiceFactory.getService(new ContactsServiceimpl());
        Boolean flag = contactsService.saveRemark(ar);

        Map<String, Object> map = new HashMap<>();
        map.put("success", flag);
        map.put("ar", ar);
        PrintJson.printJsonObj(response, map);
    }

    //对联系人备注进行添加
    private void getRemarkListByCid(HttpServletRequest request, HttpServletResponse response) {
        System.out.println("进入到根据联系人id,取得备注信息列表");

        String contactsId = request.getParameter("contactsId");


        ContactsService contactsService = (ContactsService) ServiceFactory.getService(new ContactsServiceimpl());

        List<ContactsRemark> customerRemarkList = contactsService.getRemarkListByCid(contactsId);

        PrintJson.printJsonObj(response, customerRemarkList);
    }

    //    通过名称跳转到详细页面
    private void detail(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("跳转到联系人的详细信息页面");

        String id = request.getParameter("id");

        ContactsService contactsService = (ContactsService) ServiceFactory.getService(new ContactsServiceimpl());
        Contacts c = contactsService.detail(id);
        request.setAttribute("c", c);
        request.getRequestDispatcher("/workbench/contacts/detail.jsp").forward(request, response);
    }

    //    为查询按钮添加分页查询和模糊查询并且给列表排序
    private void pageList(HttpServletRequest request, HttpServletResponse response) {
        System.out.println("进入到客户信息列表的操作(条件查询和分页查询)");

        String fullname = request.getParameter("fullname");
        String owner = request.getParameter("owner");
        String source = request.getParameter("source");
        String birth = request.getParameter("birth");
        //显示的是那一页
        String pageNoStr = request.getParameter("pageNo");
        int pageNo = Integer.parseInt(pageNoStr);
        //每页展现的记录数
        String pageSizeStr = request.getParameter("pageSize");
        int pageSize = Integer.parseInt(pageSizeStr);

        //计算出略过的记录数
        int skipCount = (pageNo - 1) * pageSize;

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("fullname", fullname);
        map.put("owner", owner);
        map.put("source", source);
        map.put("birth", birth);
        map.put("skipCount", skipCount);
        map.put("pageSize", pageSize);

        ContactsService contactsService = (ContactsService) ServiceFactory.getService(new ContactsServiceimpl());
        //设置一个vo类方便用来返回给前端的值,使用泛型可以使得这个类可以多次使用。
        PaginationVO<Contacts> vo = contactsService.pageList(map);

        PrintJson.printJsonObj(response, vo);
    }
}
