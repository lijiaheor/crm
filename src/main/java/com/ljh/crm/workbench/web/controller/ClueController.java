package com.ljh.crm.workbench.web.controller;

import com.ljh.crm.settings.pojo.User;
import com.ljh.crm.settings.service.UserService;
import com.ljh.crm.settings.service.impl.UserServiceImpl;
import com.ljh.crm.utils.DateTimeUtil;
import com.ljh.crm.utils.PrintJson;
import com.ljh.crm.utils.ServiceFactory;
import com.ljh.crm.utils.UUIDUtil;
import com.ljh.crm.vo.PaginationVO;
import com.ljh.crm.workbench.pojo.Activity;
import com.ljh.crm.workbench.pojo.Clue;
import com.ljh.crm.workbench.pojo.ClueRemark;
import com.ljh.crm.workbench.pojo.Tran;
import com.ljh.crm.workbench.service.ActivityService;
import com.ljh.crm.workbench.service.ClueService;
import com.ljh.crm.workbench.service.impl.ActivityServiceImpl;
import com.ljh.crm.workbench.service.impl.ClueServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ClueController extends HttpServlet {
    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("进入线索活动控制器");
        String path = request.getServletPath();
        if ("/workbench/clue/getUserList.do".equals(path)) {
            getUserList(request, response);
        } else if ("/workbench/clue/save.do".equals(path)) {
            save(request, response);
        } else if ("/workbench/clue/pageList.do".equals(path)) {
            pageList(request, response);
        } else if ("/workbench/clue/getUserListAndclue.do".equals(path)) {
            getUserListAndclue(request, response);
        } else if ("/workbench/clue/update.do".equals(path)) {
            update(request, response);
        } else if ("/workbench/clue/delete.do".equals(path)) {
            delete(request, response);
        } else if ("/workbench/clue/detail.do".equals(path)) {
            detail(request, response);
        } else if ("/workbench/clue/getRemarkListByAid.do".equals(path)) {
            getRemarkListByAid(request, response);
        } else if ("/workbench/clue/saveRemark.do".equals(path)) {
            saveRemark(request, response);
        } else if ("/workbench/clue/getActivityListByClueId.do".equals(path)) {
            getActivityListByClueId(request, response);
        } else if ("/workbench/clue/unbund.do".equals(path)) {
            unbund(request, response);
        } else if ("/workbench/clue/getActivityListByNameAndNotByClueId.do".equals(path)) {
            getActivityListByNameAndNotByClueId(request, response);
        } else if ("/workbench/clue/bund.do".equals(path)) {
            bund(request, response);
        } else if ("/workbench/clue/getActivityListByName.do".equals(path)) {
            getActivityListByName(request, response);
        } else if ("/workbench/clue/convert.do".equals(path)) {
            convert(request, response);
        }
    }

    //    添加线索的备注信息
    private void saveRemark(HttpServletRequest request, HttpServletResponse response) {
        System.out.println("进入到添加市场备注的操作");
        String id = UUIDUtil.getUUID();
        String clueId = request.getParameter("clueId");
        String noteContent = request.getParameter("noteContent");
        String createTime = DateTimeUtil.getSysTime();
        String createBy = ((User) request.getSession().getAttribute("user")).getName();
        String editFlag = "0";
        ClueRemark ar = new ClueRemark();
        ar.setId(id);
        ar.setClueId(clueId);
        ar.setCreateTime(createTime);
        ar.setCreateBy(createBy);
        ar.setNoteContent(noteContent);
        ar.setEditFlag(editFlag);

        ClueService clueService = (ClueService) ServiceFactory.getService(new ClueServiceImpl());
        Boolean flag = clueService.saveRemark(ar);

        Map<String, Object> map = new HashMap<>();
        map.put("success", flag);
        map.put("ar", ar);
        PrintJson.printJsonObj(response, map);
    }

    //    对备注信息信息的列表进行展示
    private void getRemarkListByAid(HttpServletRequest request, HttpServletResponse response) {
        System.out.println("进入到根据线索id,取得备注信息列表");

        String clueId = request.getParameter("clueId");

        ClueService clueService = (ClueService) ServiceFactory.getService(new ClueServiceImpl());

        List<ClueRemark> clueRemarkList = clueService.getRemarkListByAid(clueId);

        PrintJson.printJsonObj(response, clueRemarkList);
    }

    //    为修改按钮绑定事件,执行线索的修改
    private void update(HttpServletRequest request, HttpServletResponse response) {
        System.out.println("进入到根据用户id修改线索信息的操作");
        String id = request.getParameter("id");
        String fullname = request.getParameter("fullname");
        String appellation = request.getParameter("appellation");
        String owner = request.getParameter("owner");
        String company = request.getParameter("company");
        String job = request.getParameter("job");
        String email = request.getParameter("email");
        String phone = request.getParameter("phone");
        String website = request.getParameter("website");
        String mphone = request.getParameter("mphone");
        String state = request.getParameter("state");
        String source = request.getParameter("source");
        String editBy = ((User) request.getSession().getAttribute("user")).getName();
        String editTime = DateTimeUtil.getSysTime();
        String description = request.getParameter("description");
        String contactSummary = request.getParameter("contactSummary");
        String nextContactTime = request.getParameter("nextContactTime");
        String address = request.getParameter("address");
        Clue a = new Clue();
        a.setId(id);
        a.setFullname(fullname);
        a.setAppellation(appellation);
        a.setOwner(owner);
        a.setCompany(company);
        a.setJob(job);
        a.setEmail(email);
        a.setPhone(phone);
        a.setWebsite(website);
        a.setMphone(mphone);
        a.setState(state);
        a.setSource(source);
        a.setEditBy(editBy);
        a.setEditTime(editTime);
        a.setDescription(description);
        a.setContactSummary(contactSummary);
        a.setNextContactTime(nextContactTime);
        a.setAddress(address);

        ClueService clueService = (ClueService) ServiceFactory.getService(new ClueServiceImpl());

        boolean flag = clueService.update(a);

        PrintJson.printJsonFlag(response, flag);
    }

    //    为打开线索的修改模态窗口,来获取信息
    private void getUserListAndclue(HttpServletRequest request, HttpServletResponse response) {
        System.out.println("进入到查询用户信息列表和根据线索id查询单条线索记录的操作");
        String id = request.getParameter("id");
        ClueService clueService = (ClueService) ServiceFactory.getService(new ClueServiceImpl());
        Map<String, Object> map = clueService.getUserListAndclue(id);
        PrintJson.printJsonObj(response, map);
    }

    //    为转换页面中的转换按钮添加事件
    private void convert(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("执行线索转换的操作");

        String clueId = request.getParameter("clueId");
        //接收是否需要创建交易的标记
        String flag = request.getParameter("flag");

        Tran t = null;
        String createBy = null;

        //如果需要创建交易
        if ("a".equals(flag)) {
            t = new Tran();
            //接受表单中的参数
            String id = UUIDUtil.getUUID();
            String money = request.getParameter("money");
            String name = request.getParameter("name");
            String expectedDate = request.getParameter("expectedDate");
            String stage = request.getParameter("stage");
            String activityId = request.getParameter("activityId");
            createBy = ((User) request.getSession().getAttribute("user")).getName();
            String createTime = DateTimeUtil.getSysTime();
            t.setId(id);
            t.setMoney(money);
            t.setName(name);
            t.setExpectedDate(expectedDate);
            t.setStage(stage);
            t.setActivityId(activityId);
            t.setCreateBy(createBy);
            t.setCreateTime(createTime);
        }


        ClueService clueService = (ClueService) ServiceFactory.getService(new ClueServiceImpl());
        boolean flag1 = clueService.convert(clueId, t, createBy);
        if (flag1) {
            response.sendRedirect(request.getContextPath() + "/workbench/clue/index.jsp");
        }
    }

    //为在转换页面中的放大镜中的搜索框来查询市场活动的列表
    private void getActivityListByName(HttpServletRequest request, HttpServletResponse response) {
        System.out.println("查询市场活动列表(根据市场活动模糊查)");

        String aname = request.getParameter("aname");
        ActivityService as = (ActivityService) ServiceFactory.getService(new ActivityServiceImpl());
        List<Activity> aList = as.getActivityListByName(aname);
        PrintJson.printJsonObj(response, aList);
    }

    //执行删除线索的操作
    private void delete(HttpServletRequest request, HttpServletResponse response) {
        System.out.println("执行删除线索的操作");

        String ids[] = request.getParameterValues("id");
        ClueService clueService = (ClueService) ServiceFactory.getService(new ClueServiceImpl());
        boolean flag = clueService.delete(ids);
        PrintJson.printJsonFlag(response, flag);
    }

    //    进行关联市场活动中的模态窗口中的与线索绑定关联的操作
    private void bund(HttpServletRequest request, HttpServletResponse response) {
        System.out.println("执行关联市场活动的操作");
        String cid = request.getParameter("cid");
        String[] aids = request.getParameterValues("aid");

        ClueService clueService = (ClueService) ServiceFactory.getService(new ClueServiceImpl());
        boolean flag = clueService.bund(cid, aids);
        PrintJson.printJsonFlag(response, flag);
    }

    //    为关联市场活动中的模态窗口中的搜索框通过市场活动表的名字和没有关联的市场活动来进行模糊查询
    private void getActivityListByNameAndNotByClueId(HttpServletRequest request, HttpServletResponse response) {
        System.out.println("查询市场活动列表 (根据名称查询+排除已经关联指定线索的列表)");

        String aname = request.getParameter("aname");
        String clueId = request.getParameter("clueId");

        Map<String, Object> map = new HashMap<>();
        map.put("aname", aname);
        map.put("clueId", clueId);

        ActivityService as = (ActivityService) ServiceFactory.getService(new ActivityServiceImpl());
        List<Activity> aList = as.getActivityListByNameAndNotByClueId(map);
        PrintJson.printJsonObj(response, aList);
    }

    //    进行线索详细页面的解除关联操作
    private void unbund(HttpServletRequest request, HttpServletResponse response) {
        System.out.println("执行解除关联操作");
        String id = request.getParameter("id");
        ClueService clueService = (ClueService) ServiceFactory.getService(new ClueServiceImpl());
        boolean flag = clueService.unbund(id);
        PrintJson.printJsonFlag(response, flag);
    }

    //    页面加载完毕后,取出关联的市场活动信息列表
    private void getActivityListByClueId(HttpServletRequest request, HttpServletResponse response) {
        System.out.println("根据线索id查询关联的市场活动列表");
        String clueId = request.getParameter("clueId");

        ActivityService activityService = (ActivityService) ServiceFactory.getService(new ActivityServiceImpl());
        List<Activity> aList = activityService.getActivityListByClueId(clueId);

        PrintJson.printJsonObj(response, aList);
    }

    //    通过名称跳转到详细页面
    private void detail(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("跳转到线索的详细信息页面");

        String id = request.getParameter("id");

        ClueService clueService = (ClueService) ServiceFactory.getService(new ClueServiceImpl());
        Clue c = clueService.detail(id);
        request.setAttribute("c", c);
        request.getRequestDispatcher("/workbench/clue/detail.jsp").forward(request, response);
    }


    //  为添加模态窗口中的保存按钮添加事件,执行线索的添加操作
    private void save(HttpServletRequest request, HttpServletResponse response) {
        System.out.println("执行线索添加操作");

        String fullname = request.getParameter("fullname");
        String appellation = request.getParameter("appellation");
        String owner = request.getParameter("owner");
        String company = request.getParameter("company");
        String job = request.getParameter("job");
        String email = request.getParameter("email");
        String phone = request.getParameter("phone");
        String website = request.getParameter("website");
        String mphone = request.getParameter("mphone");
        String state = request.getParameter("state");
        String source = request.getParameter("source");
        String createBy = ((User) request.getSession().getAttribute("user")).getName();
        String createTime = DateTimeUtil.getSysTime();
        String description = request.getParameter("description");
        String contactSummary = request.getParameter("contactSummary");
        String nextContactTime = request.getParameter("nextContactTime");
        String address = request.getParameter("address");

        Clue c = new Clue();
        c.setId(UUIDUtil.getUUID());
        c.setFullname(fullname);
        c.setAppellation(appellation);
        c.setOwner(owner);
        c.setCompany(company);
        c.setJob(job);
        c.setEmail(email);
        c.setPhone(phone);
        c.setWebsite(website);
        c.setMphone(mphone);
        c.setState(state);
        c.setSource(source);
        c.setCreateBy(createBy);
        c.setCreateTime(createTime);
        c.setContactSummary(contactSummary);
        c.setNextContactTime(nextContactTime);
        c.setAddress(address);
        c.setDescription(description);

        ClueService clueService = (ClueService) ServiceFactory.getService(new ClueServiceImpl());
        boolean flag = clueService.save(c);
        PrintJson.printJsonFlag(response, flag);

    }

    //  为打开添加操作的模态窗口,来获取用户列表
    private void getUserList(HttpServletRequest request, HttpServletResponse response) {
        System.out.println("获取用户信息列表");
        UserService us = (UserService) ServiceFactory.getService(new UserServiceImpl());
        List<User> uList = us.getUserList();
        PrintJson.printJsonObj(response, uList);
    }

    //    为删除按钮执行删除操作
//    private void deletebatch(HttpServletRequest request, HttpServletResponse response) {
//        System.out.println("进入到线索的删除操作");
//        String[] ids = request.getParameterValues("id");
//
//        ClueService clueService = (ClueService) ServiceFactory.getService(new ClueServiceImpl());
//        boolean flag = clueService.deletebatch(ids);
//        PrintJson.printJsonFlag(response, flag);
//    }

    //    为查询按钮添加分页查询和模糊查询并且给列表排序
    private void pageList(HttpServletRequest request, HttpServletResponse response) {
        System.out.println("进入到查询线索信息列表的操作(条件查询和分页查询)");


        String fullname = request.getParameter("fullname");
        String company = request.getParameter("company");
        String phone = request.getParameter("phone");
        String mphone = request.getParameter("mphone");
        String state = request.getParameter("state");
        String owner = request.getParameter("owner");
        String source = request.getParameter("source");
        //显示是那一页
        String pageNoStr = request.getParameter("pageNo");
        int pageNo = Integer.parseInt(pageNoStr);

        //每页展现的记录数
        String pageSizeStr = request.getParameter("pageSize");
        int pageSize = Integer.parseInt(pageSizeStr);

        //计算出略过的记录数
        int skipCount = (pageNo - 1) * pageSize;
        Map<String, Object> map = new HashMap<>();
        map.put("fullname", fullname);
        map.put("company", company);
        map.put("phone", phone);
        map.put("mphone", mphone);
        map.put("state", state);
        map.put("owner", owner);
        map.put("source", source);
        map.put("skipCount", skipCount);
        map.put("pageSize", pageSize);

        ClueService clueService = (ClueService) ServiceFactory.getService(new ClueServiceImpl());

        PaginationVO<Clue> vo = clueService.pageList(map);

        PrintJson.printJsonObj(response, vo);
    }
}
