package com.ljh.crm.workbench.web.controller;

import com.ljh.crm.settings.pojo.User;
import com.ljh.crm.utils.DateTimeUtil;
import com.ljh.crm.utils.PrintJson;
import com.ljh.crm.utils.ServiceFactory;
import com.ljh.crm.utils.UUIDUtil;
import com.ljh.crm.vo.PaginationVO;
import com.ljh.crm.workbench.pojo.ActivityRemark;
import com.ljh.crm.workbench.pojo.Customer;
import com.ljh.crm.workbench.pojo.CustomerRemark;
import com.ljh.crm.workbench.service.ActivityService;
import com.ljh.crm.workbench.service.CustomerService;
import com.ljh.crm.workbench.service.impl.ActivityServiceImpl;
import com.ljh.crm.workbench.service.impl.CustomerServiceimpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CustomerController extends HttpServlet {
    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("进入交易控制器");
        String path = request.getServletPath();
        if ("/workbench/customer/pageList.do".equals(path)) {
            pageList(request, response);
        } else if ("/workbench/customer/detail.do".equals(path)) {
            detail(request, response);
        } else if ("/workbench/customer/getCharts.do".equals(path)) {
            getCharts(request, response);
        } else if ("/workbench/customer/save.do".equals(path)) {
            save(request, response);
        } else if ("/workbench/customer/getUserListAndCustomer.do".equals(path)) {
            getUserListAndCustomer(request, response);
        } else if ("/workbench/customer/update.do".equals(path)) {
            update(request, response);
        } else if ("/workbench/customer/delete.do".equals(path)) {
            delete(request, response);
        } else if ("/workbench/customer/getRemarkListByCid.do".equals(path)) {
            getRemarkListByCid(request, response);
        } else if ("/workbench/customer/saveRemark.do".equals(path)) {
            saveRemark(request, response);
        }
    }

    //对备注进行添加
    private void saveRemark(HttpServletRequest request, HttpServletResponse response) {
        System.out.println("进入到添加客户备注的操作");
        String id = UUIDUtil.getUUID();
        String customerId = request.getParameter("customerId");
        String noteContent = request.getParameter("noteContent");
        String createTime = DateTimeUtil.getSysTime();
        String createBy = ((User) request.getSession().getAttribute("user")).getName();
        String editFlag = "0";
        CustomerRemark ar = new CustomerRemark();
        ar.setId(id);
        ar.setCustomerId(customerId);
        ar.setCreateTime(createTime);
        ar.setCreateBy(createBy);
        ar.setNoteContent(noteContent);
        ar.setEditFlag(editFlag);

        CustomerService customerService = (CustomerService) ServiceFactory.getService(new CustomerServiceimpl());
        Boolean flag = customerService.saveRemark(ar);

        Map<String, Object> map = new HashMap<>();
        map.put("success", flag);
        map.put("ar", ar);
        PrintJson.printJsonObj(response, map);
    }

    //    对备注信息信息的列表进行展示
    private void getRemarkListByCid(HttpServletRequest request, HttpServletResponse response) {
        System.out.println("进入到根据客户id,取得备注信息列表");

        String customerId = request.getParameter("customerId");

        CustomerService customerService = (CustomerService) ServiceFactory.getService(new CustomerServiceimpl());

        List<CustomerRemark> customerRemarkList = customerService.getRemarkListByCid(customerId);

        PrintJson.printJsonObj(response, customerRemarkList);

    }

    //    删除客户信息(多个或一个)
    private void delete(HttpServletRequest request, HttpServletResponse response) {
        System.out.println("执行删除客户的操作");

        String ids[] = request.getParameterValues("id");
        CustomerService customerService = (CustomerService) ServiceFactory.getService(new CustomerServiceimpl());
        boolean flag = customerService.delete(ids);
        PrintJson.printJsonFlag(response, flag);
    }

    //修改客户信息
    private void update(HttpServletRequest request, HttpServletResponse response) {
        System.out.println("进入到根据用户id修改客户信息的操作");
        String id = request.getParameter("id");
        String owner = request.getParameter("owner");
        String name = request.getParameter("name");
        String website = request.getParameter("website");
        String phone = request.getParameter("phone");
        String contactSummary = request.getParameter("contactSummary");
        String nextContactTime = request.getParameter("nextContactTime");
        String description = request.getParameter("description");
        String address = request.getParameter("address");
        String editBy = ((User) request.getSession().getAttribute("user")).getName();
        String editTime = DateTimeUtil.getSysTime();

        Customer a = new Customer();
        a.setId(id);
        a.setName(name);
        a.setOwner(owner);
        a.setPhone(phone);
        a.setWebsite(website);
        a.setEditBy(editBy);
        a.setEditTime(editTime);
        a.setDescription(description);
        a.setContactSummary(contactSummary);
        a.setNextContactTime(nextContactTime);
        a.setAddress(address);

        CustomerService customerService = (CustomerService) ServiceFactory.getService(new CustomerServiceimpl());

        boolean flag = customerService.update(a);

        PrintJson.printJsonFlag(response, flag);
    }

    //    为打开客户的修改模态窗口,来获取信息
    private void getUserListAndCustomer(HttpServletRequest request, HttpServletResponse response) {

        System.out.println("进入到查询用户信息列表和根据线索id查询单条客户记录的操作");
        String id = request.getParameter("id");
        CustomerService customerService = (CustomerService) ServiceFactory.getService(new CustomerServiceimpl());
        Map<String, Object> map = customerService.getUserListAndCustomer(id);
        PrintJson.printJsonObj(response, map);
    }

    //添加客户
    private void save(HttpServletRequest request, HttpServletResponse response) {
        System.out.println("执行客户添加操作");

        String owner = request.getParameter("owner");
        String name = request.getParameter("name");
        String website = request.getParameter("website");
        String phone = request.getParameter("phone");
        String contactSummary = request.getParameter("contactSummary");
        String nextContactTime = request.getParameter("nextContactTime");
        String description = request.getParameter("description");
        String address = request.getParameter("address");
        String createBy = ((User) request.getSession().getAttribute("user")).getName();
        String createTime = DateTimeUtil.getSysTime();

        Customer c = new Customer();
        c.setId(UUIDUtil.getUUID());
        c.setOwner(owner);
        c.setName(name);
        c.setPhone(phone);
        c.setWebsite(website);
        c.setCreateBy(createBy);
        c.setDescription(description);
        c.setCreateTime(createTime);
        c.setContactSummary(contactSummary);
        c.setNextContactTime(nextContactTime);
        c.setAddress(address);

        CustomerService customerService = (CustomerService) ServiceFactory.getService(new CustomerServiceimpl());
        boolean flag = customerService.save(c);
        PrintJson.printJsonFlag(response, flag);
    }

    //    进行客户和联系人的数量统计
    private void getCharts(HttpServletRequest request, HttpServletResponse response) {
        System.out.println("进入客户和联系人的数量的统计(取得交易阶段数量)");

        CustomerService customerService = (CustomerService) ServiceFactory.getService(new CustomerServiceimpl());
        Map<String, Object> map = customerService.getCharts();
        PrintJson.printJsonObj(response, map);
    }

    //    通过名称跳转到详细页面
    private void detail(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("跳转到客户的详细信息页面");

        String id = request.getParameter("id");

        CustomerService customerService = (CustomerService) ServiceFactory.getService(new CustomerServiceimpl());
        Customer c = customerService.detail(id);
        request.setAttribute("c", c);
        request.getRequestDispatcher("/workbench/customer/detail.jsp").forward(request, response);

    }

    //    为查询按钮添加分页查询和模糊查询并且给列表排序
    private void pageList(HttpServletRequest request, HttpServletResponse response) {
        System.out.println("进入到客户信息列表的操作(条件查询和分页查询)");

        String name = request.getParameter("name");
        String owner = request.getParameter("owner");
        String phone = request.getParameter("phone");
        String website = request.getParameter("website");
        //显示的是那一页
        String pageNoStr = request.getParameter("pageNo");
        int pageNo = Integer.parseInt(pageNoStr);
        //每页展现的记录数
        String pageSizeStr = request.getParameter("pageSize");
        int pageSize = Integer.parseInt(pageSizeStr);

        //计算出略过的记录数
        int skipCount = (pageNo - 1) * pageSize;

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("name", name);
        map.put("owner", owner);
        map.put("phone", phone);
        map.put("website", website);
        map.put("skipCount", skipCount);
        map.put("pageSize", pageSize);

        CustomerService customerService = (CustomerService) ServiceFactory.getService(new CustomerServiceimpl());
        //设置一个vo类方便用来返回给前端的值,使用泛型可以使得这个类可以多次使用。
        PaginationVO<Customer> vo = customerService.pageList(map);

        PrintJson.printJsonObj(response, vo);
    }
}
