package com.ljh.crm.workbench.web.controller;

import com.ljh.crm.utils.PrintJson;
import com.ljh.crm.utils.ServiceFactory;
import com.ljh.crm.vo.PaginationVO;
import com.ljh.crm.workbench.pojo.Tran;
import com.ljh.crm.workbench.pojo.TranHistory;
import com.ljh.crm.workbench.service.TranService;
import com.ljh.crm.workbench.service.impl.TranServiceImpl;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TranController extends HttpServlet {
    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("进入交易控制器");
        String path = request.getServletPath();
        if ("/workbench/transaction/getCharts.do".equals(path)) {
            getCharts(request, response);
        } else if ("/workbench/transaction/pageList.do".equals(path)) {
            pageList(request, response);
        } else if ("/workbench/transaction/detail.do".equals(path)) {
            detail(request, response);
        } else if ("/workbench/transaction/delete.do".equals(path)) {
            delete(request, response);
        } else if ("/workbench/transaction/getHistoryListByTranId.do".equals(path)) {
            getHistoryListByTranId(request, response);
        }
    }

    //    在页面加载完毕后，展现交易历史列表
    private void getHistoryListByTranId(HttpServletRequest request, HttpServletResponse response) {
        System.out.println("根据交易id取得相应的历史列表");

        String tranId = request.getParameter("tranId");

        TranService ts = (TranService) ServiceFactory.getService(new TranServiceImpl());

        List<TranHistory> thList= ts.getHistoryListByTranId(tranId);

        PrintJson.printJsonObj(response, thList);
    }
    //进行交易的删除操作
    private void delete(HttpServletRequest request, HttpServletResponse response) {
        System.out.println("执行删除联系人的操作");

        String ids[] = request.getParameterValues("id");
        TranService tranService = (TranService) ServiceFactory.getService(new TranServiceImpl());
        boolean flag = tranService.delete(ids);
        PrintJson.printJsonFlag(response, flag);
    }

    //    通过名称跳转到详细页面
    private void detail(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("跳转到交易的详细信息页面");

        String id = request.getParameter("id");

        TranService tranService = (TranService) ServiceFactory.getService(new TranServiceImpl());
        Tran  c = tranService.detail(id);
        request.setAttribute("c", c);
        request.getRequestDispatcher("/workbench/transaction/detail.jsp").forward(request, response);
    }

    //    为查询按钮添加分页查询和模糊查询并且给列表排序
    private void pageList(HttpServletRequest request, HttpServletResponse response) {
        System.out.println("进入到交易信息列表的操作(条件查询和分页查询)");

        String owner = request.getParameter("owner");
        String name = request.getParameter("name");
        String customerId = request.getParameter("customerId");
        String stage = request.getParameter("stage");
        String type = request.getParameter("type");
        String source = request.getParameter("source");
        String contactsId = request.getParameter("contactsId");
        //显示的是那一页
        String pageNoStr = request.getParameter("pageNo");
        int pageNo = Integer.parseInt(pageNoStr);
        //每页展现的记录数
        String pageSizeStr = request.getParameter("pageSize");
        int pageSize = Integer.parseInt(pageSizeStr);

        //计算出略过的记录数
        int skipCount = (pageNo - 1) * pageSize;

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("owner", owner);
        map.put("name", name);
        map.put("customerId", customerId);
        map.put("stage", stage);
        map.put("type", type);
        map.put("source", source);
        map.put("contactsId", contactsId);
        map.put("skipCount", skipCount);
        map.put("pageSize", pageSize);

        TranService tranService = (TranService) ServiceFactory.getService(new TranServiceImpl());
        //设置一个vo类方便用来返回给前端的值,使用泛型可以使得这个类可以多次使用。
        PaginationVO<Tran> vo = tranService.pageList(map);

        PrintJson.printJsonObj(response, vo);
    }

    //建立交易的统计漏斗图
    private void getCharts(HttpServletRequest request, HttpServletResponse response) {
        System.out.println("进入到交易图表的统计(取得交易阶段数量)");

        TranService tranService = (TranService) ServiceFactory.getService(new TranServiceImpl());
        Map<String, Object> map = tranService.getCharts();
        PrintJson.printJsonObj(response, map);
    }
}
