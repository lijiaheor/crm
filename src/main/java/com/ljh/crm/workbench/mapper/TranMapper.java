package com.ljh.crm.workbench.mapper;

import com.ljh.crm.workbench.pojo.Tran;
import com.ljh.crm.workbench.pojo.TranHistory;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface TranMapper {
    //添加交易
    int save(Tran t);

    //获取交易总数量
    int getTotal();

    //获取到每个交易的数量和名称
    List<Map<String, Object>> getCharts();

    //    查询交易的信息总条数
    int getTotalByCondition(Map<String, Object> map);

    //    查询交易的相关信息
    List<Tran> gettranByCondition(Map<String, Object> map);

    Tran detail(String id);

    //    进行交易的删除操作
    int deletebatch(String[] ids);

    //    在页面加载完毕后，展现交易历史列表
    List<TranHistory> getHistoryListByTranId(String tranId);
}
