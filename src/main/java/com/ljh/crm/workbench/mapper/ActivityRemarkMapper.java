package com.ljh.crm.workbench.mapper;

import com.ljh.crm.workbench.pojo.ActivityRemark;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ActivityRemarkMapper {
    //查询出需要删除的备注数量
    int getCountByAids(String[] ids);

    //删除备注(实际删除的备注数量)
    int deleteByAids(String[] ids);

    //进入到市场活动页面后进行加载备注信息列表
    List<ActivityRemark> getRemarkListByAid(String activityId);
    //为进入到市场活动详细页面后对备注进行删除操作
    int deleteRemark(String id);
    // 为进入到市场活动详细页面后对备注进行添加操作
    int saveRemark(ActivityRemark ar);
    //为进入到市场活动详细页面后对备注进行修改操作
    int updateRemark(ActivityRemark ar);
}
