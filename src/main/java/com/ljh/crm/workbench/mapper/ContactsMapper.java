package com.ljh.crm.workbench.mapper;

import com.ljh.crm.workbench.pojo.Contacts;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface ContactsMapper {
    //添加联系人
    int save(Contacts con);

    //    查询市场活动的信息总条数
    int getTotalByCondition(Map<String, Object> map);

    //    查询联系人的相关信息
    List<Contacts> getcontactsByCondition(Map<String, Object> map);

    //    通过名称跳转到详细页面
    Contacts detail(String id);

    //查询联系人的总数量
    int getconTotal();

    // 为修改按钮绑定事件，打开修改操作的模态窗口
    Contacts getId(String id);

    //    进行联系人信息的修改
    int update(Contacts a);

    //    进行联系人的删除操作
    int deletebatch(String[] ids);
}
