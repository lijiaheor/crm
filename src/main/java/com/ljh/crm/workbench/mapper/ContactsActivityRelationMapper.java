package com.ljh.crm.workbench.mapper;

import com.ljh.crm.workbench.pojo.ContactsActivityRelation;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ContactsActivityRelationMapper {
    //    创建 联系人与市场活动的关联关系对象
    int save(ContactsActivityRelation contactsActivityRelation);
}
