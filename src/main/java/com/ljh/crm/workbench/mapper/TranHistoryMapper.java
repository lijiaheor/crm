package com.ljh.crm.workbench.mapper;

import com.ljh.crm.workbench.pojo.TranHistory;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface TranHistoryMapper {
    //添加交易历史
    int save(TranHistory th);
}
