package com.ljh.crm.workbench.mapper;

import com.ljh.crm.workbench.pojo.Customer;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface CustomerMapper {
    //通过获取一个公司的名字来判断客户表中有没有这个公司
    Customer getCustomerByName(String company);

    //    添加客户
    int save(Customer cus);

    //查询客户的信息总条数
    int getTotalByCondition(Map<String, Object> map);

    //查询客户的相关信息
    List<Customer> getCustomerByCondition(Map<String, Object> map);

    //    通过名称跳转到详细页面
    Customer detail(String id);


    //查询客户的总数量
    int getcusTotal();

    //    为打开客户的修改模态窗口,来获取信息
    Customer getId(String id);

    //修改客户信息
    int update(Customer a);

    //    删除客户信息
    int deletebatch(String[] ids);
}
