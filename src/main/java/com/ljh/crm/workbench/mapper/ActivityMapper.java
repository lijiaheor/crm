package com.ljh.crm.workbench.mapper;

import com.ljh.crm.workbench.pojo.Activity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface ActivityMapper {
    //添加市场活动信息
    int save(Activity a);

    //查询市场活动的信息总条数
    int getTotalByCondition(Map<String, Object> map);

    //查询市场活动的相关信息
    List<Activity> getActivityByCondition(Map<String, Object> map);

    //删除相关id的市场活动信息
    int delete(String[] ids);

    //    打开修改模态窗口时显示的信息(查询用户信息列表和根据市场活动id查询单条记录的操作)
    Activity getById(String id);

    //  根据用户id修改用户市场活动的相关信息
    int update(Activity a);

    //市场活动信息跳转到详细信息页面
    Activity detail(String id);

    //    页面加载完毕后,取出关联的市场活动信息列表
    List<Activity> getActivityListByClueId(String clueId);

    //    为关联市场活动中的模态窗口中的搜索框通过市场活动表的名字和没有关联的市场活动来进行模糊查询
    List<Activity> getActivityListByNameAndNotByClueId(Map<String, Object> map);

    //    为在转换页面中的放大镜中的搜索框来查询市场活动的列表
    List<Activity> getActivityListByName(String aname);


    List<String> getnCharts();

    List<String> getcCharts();
}
