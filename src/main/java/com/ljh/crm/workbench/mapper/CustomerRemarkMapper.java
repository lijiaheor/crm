package com.ljh.crm.workbench.mapper;

import com.ljh.crm.workbench.pojo.CustomerRemark;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface CustomerRemarkMapper {
    //    添加客户备注
    int save(CustomerRemark customerRemark);

    List<CustomerRemark> getRemarkListByAid(String customerId);
    //对备注进行添加
    int saveRemark(CustomerRemark ar);
}


