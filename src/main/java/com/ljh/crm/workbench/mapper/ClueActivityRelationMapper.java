package com.ljh.crm.workbench.mapper;


import com.ljh.crm.workbench.pojo.ClueActivityRelation;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ClueActivityRelationMapper {

    //    进行线索详细页面的解除关联操作
    int unbund(String id);

    //    进行关联市场活动中的模态窗口中的与线索绑定关联的操作
    int bund(ClueActivityRelation car);

    //    删除线索与活动的关联关联表(用市场活动的id来删除线索与市场活动的关联表)
    int deletebatch(String[] activityid);

    //根据线索id来获取线索与市场活动列表
    List<ClueActivityRelation> getListByClueId(String clueId);
    //(9) 删除线索和市场活动的关系
    int delete(ClueActivityRelation clueActivityRelation);
}
