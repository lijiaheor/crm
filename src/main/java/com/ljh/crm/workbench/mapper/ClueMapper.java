package com.ljh.crm.workbench.mapper;

import com.ljh.crm.workbench.pojo.Clue;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;


@Mapper
public interface ClueMapper {

    //为添加模态窗口中的保存按钮添加事件,执行线索的添加操作
    int save(Clue c);

    //查询线索的信息总条数
    int getTotalByCondition(Map<String, Object> map);
    //查询线索的相关信息

    List<Clue> getclueByCondition(Map<String, Object> map);

    //删除市场活动信息
    int deletebatch(String[] ids);

    //    通过名称跳转到详细页面
    Clue detail(String id);

    //(1)通过线索id获取线索对象（线索对象当中封装了线索的信息）
    Clue getId(String clueId);

    //(10) 删除线索
    int delete(String clueId);

    //    为修改按钮绑定事件,执行线索的修改
    int update(Clue a);
}
