package com.ljh.crm.workbench.mapper;

import com.ljh.crm.workbench.pojo.ContactsRemark;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ContactsRemarkMapper {
    //    添加联系人备注
    int save(ContactsRemark contactsRemark);

    //    对联系人备注进行添加
    List<ContactsRemark> getRemarkListByAid(String contactsId);

    int saveRemark(ContactsRemark ar);
}
