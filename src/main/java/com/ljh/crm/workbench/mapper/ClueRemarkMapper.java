package com.ljh.crm.workbench.mapper;

import com.ljh.crm.workbench.pojo.ClueRemark;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ClueRemarkMapper {
    //根据线索id来获取线索的备注信息列表
    List<ClueRemark> getListByClueId(String clueId);

    //(8)删除线索备注
    int delete(ClueRemark clueRemark);


    //查询出需要删除的备注数量
    int getCountByAids(String[] ids);

    //删除备注,返回收到的影响行数
    int deleteByAids(String[] ids);

    //    对备注信息信息的列表进行展示
    List<ClueRemark> getRemarkListByAid(String clueId);

    //    添加线索的备注信息
    int saveRemark(ClueRemark ar);
}
