package com.ljh.crm.workbench.pojo;

import java.util.Objects;

/**
 * 市场活动备注表
 */
public class ActivityRemark {
    private String id;
    private String noteContent;//备注信息
    private String createTime;//创建的时间
    private String createBy;//创建人
    private String editTime;//修改时间
    private String editBy;//修改人
    private String editFlag;//是否修改过的标记
    private String activityId;

    public ActivityRemark() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNoteContent() {
        return noteContent;
    }

    public void setNoteContent(String noteContent) {
        this.noteContent = noteContent;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public String getEditTime() {
        return editTime;
    }

    public void setEditTime(String editTime) {
        this.editTime = editTime;
    }

    public String getEditBy() {
        return editBy;
    }

    public void setEditBy(String editBy) {
        this.editBy = editBy;
    }

    public String getEditFlag() {
        return editFlag;
    }

    public void setEditFlag(String editFlag) {
        this.editFlag = editFlag;
    }

    public String getActivityId() {
        return activityId;
    }

    public void setActivityId(String activityId) {
        this.activityId = activityId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ActivityRemark)) return false;
        ActivityRemark that = (ActivityRemark) o;
        return id == that.id && activityId == that.activityId && Objects.equals(noteContent, that.noteContent) && Objects.equals(createTime, that.createTime) && Objects.equals(createBy, that.createBy) && Objects.equals(editTime, that.editTime) && Objects.equals(editBy, that.editBy) && Objects.equals(editFlag, that.editFlag);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, noteContent, createTime, createBy, editTime, editBy, editFlag, activityId);
    }

    @Override
    public String toString() {
        return "ActivityRemark{" +
                "id=" + id +
                ", noteContent='" + noteContent + '\'' +
                ", createTime='" + createTime + '\'' +
                ", createBy='" + createBy + '\'' +
                ", editTime='" + editTime + '\'' +
                ", editBy='" + editBy + '\'' +
                ", editFlag='" + editFlag + '\'' +
                ", activityId='" + activityId + '\'' +
                '}';
    }
}
