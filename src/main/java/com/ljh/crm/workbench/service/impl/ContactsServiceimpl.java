package com.ljh.crm.workbench.service.impl;

import com.ljh.crm.settings.mapper.UserMapper;
import com.ljh.crm.settings.pojo.User;
import com.ljh.crm.utils.SqlSessionUtil;
import com.ljh.crm.vo.PaginationVO;
import com.ljh.crm.workbench.mapper.ContactsMapper;
import com.ljh.crm.workbench.mapper.ContactsRemarkMapper;
import com.ljh.crm.workbench.pojo.*;
import com.ljh.crm.workbench.service.ContactsService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ContactsServiceimpl implements ContactsService {
    private UserMapper userMapper = SqlSessionUtil.getSqlSession().getMapper(UserMapper.class);
    private ContactsMapper contactsMapper = SqlSessionUtil.getSqlSession().getMapper(ContactsMapper.class);

    private ContactsRemarkMapper contactsRemarkMapper = SqlSessionUtil.getSqlSession().getMapper(ContactsRemarkMapper.class);
    @Override
    public PaginationVO<Contacts> pageList(Map<String, Object> map) {
        //取得total
        int total = contactsMapper.getTotalByCondition(map);
        //取得dataList
        List<Contacts> dataList = contactsMapper.getcontactsByCondition(map);
        //创建一个vo对象,将total和dataList封装到vo中
        PaginationVO<Contacts> vo = new PaginationVO<Contacts>();

        vo.setTotal(total);
        vo.setDataList(dataList);

        return vo;
    }

    @Override
    public Contacts detail(String id) {
        Contacts c = contactsMapper.detail(id);
        return c;
    }

    @Override
    public List<ContactsRemark> getRemarkListByCid(String contactsId) {
        List<ContactsRemark> arList = contactsRemarkMapper.getRemarkListByAid(contactsId);
        return arList;
    }

    @Override
    public Boolean saveRemark(ContactsRemark ar) {
        boolean flag = true;
        int count = contactsRemarkMapper.saveRemark(ar);
        if (count != 1) {
            flag = false;
        }
        return flag;
    }

    @Override
    public boolean save(Contacts c) {
        boolean flag = true;
        int count = contactsMapper.save(c);
        if (count != 1) {
            flag = false;
        }
        return flag;
    }

    @Override
    public Map<String, Object> getUserListAndCustomer(String id) {
        List<User> uList = userMapper.getUserList();
        Contacts a = contactsMapper.getId(id);
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("uList", uList);
        map.put("a", a);
        return map;
    }

    @Override
    public boolean update(Contacts a) {
        boolean flag = true;

        int count = contactsMapper.update(a);

        if (count != 1) {
            flag = false;
        }
        return flag;
    }

    @Override
    public boolean delete(String[] ids) {
        boolean flag = true;
        //删除联系人信息
        int count3 = contactsMapper.deletebatch(ids);
        if (count3 != ids.length) {
            flag = false;
        }
        return flag;
    }
}
