package com.ljh.crm.workbench.service.impl;

import com.ljh.crm.settings.mapper.UserMapper;
import com.ljh.crm.settings.pojo.User;
import com.ljh.crm.utils.SqlSessionUtil;
import com.ljh.crm.vo.PaginationVO;
import com.ljh.crm.workbench.mapper.ActivityMapper;
import com.ljh.crm.workbench.mapper.ActivityRemarkMapper;
import com.ljh.crm.workbench.mapper.ClueActivityRelationMapper;
import com.ljh.crm.workbench.pojo.Activity;
import com.ljh.crm.workbench.pojo.ActivityRemark;
import com.ljh.crm.workbench.service.ActivityService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ActivityServiceImpl implements ActivityService {
    //    调用dao/mapper层
    private ActivityMapper activityMapper = SqlSessionUtil.getSqlSession().getMapper(ActivityMapper.class);
    private ActivityRemarkMapper activityRemarkMapper = SqlSessionUtil.getSqlSession().getMapper(ActivityRemarkMapper.class);
    private UserMapper userMapper = SqlSessionUtil.getSqlSession().getMapper(UserMapper.class);
    private ClueActivityRelationMapper clueActivityRelationMapper = SqlSessionUtil.getSqlSession().getMapper(ClueActivityRelationMapper.class);

    @Override
    public Boolean updateRemark(ActivityRemark ar) {
        boolean flag = true;
        int count = activityRemarkMapper.updateRemark(ar);
        if (count != 1) {
            flag = false;
        }
        return flag;
    }

    @Override
    public List<Activity> getActivityListByClueId(String clueId) {
        List<Activity> aList =  activityMapper.getActivityListByClueId(clueId);
        return aList;
    }

    @Override
    public List<Activity> getActivityListByNameAndNotByClueId(Map<String, Object> map) {
        List<Activity> aList = activityMapper.getActivityListByNameAndNotByClueId(map);
        return aList;
    }

    @Override
    public List<Activity> getActivityListByName(String aname) {
        List<Activity> aList = activityMapper.getActivityListByName(aname);
        return aList;
    }

    @Override
    public Map<String, Object> getCharts() {

        List<String> nameList = activityMapper.getnCharts();
        //取得dataList
        List<String> costList = activityMapper.getcCharts();
        //放入到Map集合中
        Map<String, Object> map = new HashMap<>();
        map.put("nameList", nameList);
        map.put("costList", costList);
        return map;
    }

    @Override
    public Boolean saveRemark(ActivityRemark ar) {
        boolean flag = true;
        int count = activityRemarkMapper.saveRemark(ar);
        if (count != 1) {
            flag = false;
        }
        return flag;
    }


    @Override
    public boolean deleteRemark(String id) {
        boolean flag = true;
        int count = activityRemarkMapper.deleteRemark(id);
        if (count != 1) {
            flag = false;
        }
        return flag;
    }


    @Override
    public List<ActivityRemark> getRemarkListByAid(String activityId) {
        List<ActivityRemark> arList = activityRemarkMapper.getRemarkListByAid(activityId);
        return arList;
    }
    @Override
    public Activity detail(String id) {
        Activity a = activityMapper.detail(id);
        return a;
    }


    @Override
    public boolean update(Activity a) {
        boolean flag = true;

        int count = activityMapper.update(a);

        if (count != 1) {
            flag = false;
        }
        return flag;
    }


    @Override
    public Map<String, Object> getUserListAndActivity(String id) {

        List<User> uList = userMapper.getUserList();
        Activity a = activityMapper.getById(id);
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("uList", uList);
        map.put("a", a);
        return map;
    }

    @Override
    public boolean delete(String[] ids) {
        boolean flag = true;
        //查询出需要删除的备注数量
        int count1 = activityRemarkMapper.getCountByAids(ids);
        //删除备注,返回收到的影响行数(实际删除的行数)
        int count2 = activityRemarkMapper.deleteByAids(ids);
        if (count1 != count2) {
            flag = false;
        }
        //删除市场活动和线索关联的表
        int count4 = clueActivityRelationMapper.deletebatch(ids);
        if(count4 != 1){
            flag = false;
        }
        //删除市场活动
        int count3 = activityMapper.delete(ids);
        if (count3 != ids.length) {
            flag = false;
        }

        return flag;
    }

    @Override
    public boolean save(Activity a) {

        boolean flag = true;

        int count = activityMapper.save(a);

        if (count != 1) {
            flag = false;
        }
        return flag;
    }

    @Override
    public PaginationVO<Activity> pageList(Map<String, Object> map) {
        //取得total
        int total = activityMapper.getTotalByCondition(map);
        //取得dataList
        List<Activity> dataList = activityMapper.getActivityByCondition(map);
        //创建一个vo对象,将total和dataList封装到vo中
        PaginationVO<Activity> vo = new PaginationVO<Activity>();

        vo.setTotal(total);
        vo.setDataList(dataList);

        return vo;
    }


}
