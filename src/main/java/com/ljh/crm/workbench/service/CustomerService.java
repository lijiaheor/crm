package com.ljh.crm.workbench.service;

import com.ljh.crm.vo.PaginationVO;
import com.ljh.crm.workbench.pojo.Customer;
import com.ljh.crm.workbench.pojo.CustomerRemark;

import java.util.List;
import java.util.Map;

public interface CustomerService {
    //    为查询按钮添加分页查询和模糊查询并且给列表排序
    PaginationVO<Customer> pageList(Map<String, Object> map);

    //    通过名称跳转到详细页面
    Customer detail(String id);

    //    进行客户和联系人的数量统计
    Map<String, Object> getCharts();

    //添加客户信息
    boolean save(Customer c);

    //    为打开客户的修改模态窗口,来获取信息
    Map<String, Object> getUserListAndCustomer(String id);

    //    修改客户信息
    boolean update(Customer a);

    //    删除客户信息(多个或一个)
    boolean delete(String[] ids);

    //    对备注信息信息的列表进行展示
    List<CustomerRemark> getRemarkListByCid(String customerId);
    //对备注进行添加
    Boolean saveRemark(CustomerRemark ar);
}
