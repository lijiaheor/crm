package com.ljh.crm.workbench.service.impl;

import com.ljh.crm.settings.mapper.UserMapper;
import com.ljh.crm.settings.pojo.User;
import com.ljh.crm.utils.SqlSessionUtil;
import com.ljh.crm.vo.PaginationVO;
import com.ljh.crm.workbench.mapper.ContactsMapper;
import com.ljh.crm.workbench.mapper.CustomerMapper;
import com.ljh.crm.workbench.mapper.CustomerRemarkMapper;
import com.ljh.crm.workbench.pojo.*;
import com.ljh.crm.workbench.service.CustomerService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CustomerServiceimpl implements CustomerService {

    private UserMapper userMapper = SqlSessionUtil.getSqlSession().getMapper(UserMapper.class);
    private CustomerMapper customerMapper = SqlSessionUtil.getSqlSession().getMapper(CustomerMapper.class);
    private ContactsMapper contactsMapper = SqlSessionUtil.getSqlSession().getMapper(ContactsMapper.class);

    private CustomerRemarkMapper customerRemarkMapper = SqlSessionUtil.getSqlSession().getMapper(CustomerRemarkMapper.class);
    @Override
    public PaginationVO<Customer> pageList(Map<String, Object> map) {
        //取得total
        int total = customerMapper.getTotalByCondition(map);
        //取得dataList
        List<Customer> dataList = customerMapper.getCustomerByCondition(map);
        //创建一个vo对象,将total和dataList封装到vo中
        PaginationVO<Customer> vo = new PaginationVO<Customer>();

        vo.setTotal(total);
        vo.setDataList(dataList);

        return vo;
    }

    @Override
    public Customer detail(String id) {
        Customer c = customerMapper.detail(id);
        return c;
    }

    @Override
    public Map<String, Object> getCharts() {
        //取得total
        int custotal = customerMapper.getcusTotal();
        //取得dataList
        int contotal = contactsMapper.getconTotal();
        //放入到Map集合中
        Map<String, Object> map = new HashMap<>();
        map.put("custotal", custotal);
        map.put("contotal", contotal);
        return map;
    }

    @Override
    public boolean save(Customer c) {
        boolean flag = true;
        int count = customerMapper.save(c);
        if (count != 1) {
            flag = false;
        }
        return flag;
    }

    @Override
    public Map<String, Object> getUserListAndCustomer(String id) {
        List<User> uList = userMapper.getUserList();
        Customer a = customerMapper.getId(id);
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("uList", uList);
        map.put("a", a);
        return map;
    }

    @Override
    public boolean update(Customer a) {
        boolean flag = true;

        int count = customerMapper.update(a);

        if (count != 1) {
            flag = false;
        }
        return flag;
    }

    @Override
    public boolean delete(String[] ids) {
        boolean flag = true;
        //删除客户信息
        int count3 = customerMapper.deletebatch(ids);
        if (count3 != ids.length) {
            flag = false;
        }
        return flag;
    }

    @Override
    public List<CustomerRemark> getRemarkListByCid(String customerId) {
        List<CustomerRemark> arList = customerRemarkMapper.getRemarkListByAid(customerId);
        return arList;
    }

    @Override
    public Boolean saveRemark(CustomerRemark ar) {
        boolean flag = true;
        int count = customerRemarkMapper.saveRemark(ar);
        if (count != 1) {
            flag = false;
        }
        return flag;
    }
}
