package com.ljh.crm.workbench.service;

import com.ljh.crm.vo.PaginationVO;
import com.ljh.crm.workbench.pojo.Activity;
import com.ljh.crm.workbench.pojo.ActivityRemark;

import java.util.List;
import java.util.Map;

public interface ActivityService {
    //   执行市场活动保存按钮执行添加操作
    boolean save(Activity a);

    //   执行市场活动的查询操作(分页查询 加条件查询)
    PaginationVO<Activity> pageList(Map<String, Object> map);

    //   执行市场活动中的删除操作
    boolean delete(String[] ids);

    //    打开修改模态窗口时显示的信息(查询用户信息列表和根据市场活动id查询单条记录的操作)
    Map<String, Object> getUserListAndActivity(String id);

    //  根据用户id修改用户的市场活动信息
    boolean update(Activity a);

    //市场活动信息跳转到详细信息页面
    Activity detail(String id);

    //进入到市场活动页面后进行加载备注信息列表
    List<ActivityRemark> getRemarkListByAid(String activityId);

    //为进入到市场活动详细页面后对备注进行删除操作
    boolean deleteRemark(String id);

    // 为进入到市场活动详细页面后对备注进行添加操作
    Boolean saveRemark(ActivityRemark ar);

    //为进入到市场活动详细页面后对备注进行修改操作
    Boolean updateRemark(ActivityRemark ar);

    //    页面加载完毕后,取出关联的市场活动信息列表
    List<Activity> getActivityListByClueId(String clueId);

    //    为关联市场活动中的模态窗口中的搜索框通过市场活动表的名字和没有关联的市场活动来进行模糊查询
    List<Activity> getActivityListByNameAndNotByClueId(Map<String, Object> map);

    //    为在转换页面中的放大镜中的搜索框来查询市场活动的列表
    List<Activity> getActivityListByName(String aname);
//    为市场活动的名称和成本制作柱状图
    Map<String, Object> getCharts();
}
