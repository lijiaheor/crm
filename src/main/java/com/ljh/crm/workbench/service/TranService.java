package com.ljh.crm.workbench.service;

import com.ljh.crm.vo.PaginationVO;
import com.ljh.crm.workbench.pojo.Tran;
import com.ljh.crm.workbench.pojo.TranHistory;

import java.util.List;
import java.util.Map;

public interface TranService {
    //    交易图表的统计(取得交易阶段数量)
    Map<String, Object> getCharts();

    //    为查询按钮添加分页查询和模糊查询并且给列表排序
    PaginationVO<Tran> pageList(Map<String, Object> map);

    Tran detail(String id);

    //    进行交易的删除操作
    boolean delete(String[] ids);

    //    在页面加载完毕后，展现交易历史列表
    List<TranHistory> getHistoryListByTranId(String tranId);
}
