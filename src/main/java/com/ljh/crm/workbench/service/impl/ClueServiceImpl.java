package com.ljh.crm.workbench.service.impl;

import com.ljh.crm.settings.mapper.UserMapper;
import com.ljh.crm.settings.pojo.User;
import com.ljh.crm.utils.DateTimeUtil;
import com.ljh.crm.utils.SqlSessionUtil;
import com.ljh.crm.utils.UUIDUtil;
import com.ljh.crm.vo.PaginationVO;
import com.ljh.crm.workbench.mapper.*;
import com.ljh.crm.workbench.pojo.*;
import com.ljh.crm.workbench.service.ClueService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ClueServiceImpl implements ClueService {
    private UserMapper userMapper = SqlSessionUtil.getSqlSession().getMapper(UserMapper.class);
    //线索相关的三个表
    private ClueMapper clueMapper = SqlSessionUtil.getSqlSession().getMapper(ClueMapper.class);
    private ClueRemarkMapper clueRemarkMapper = SqlSessionUtil.getSqlSession().getMapper(ClueRemarkMapper.class);
    private ClueActivityRelationMapper clueActivityRelationMapper = SqlSessionUtil.getSqlSession().getMapper(ClueActivityRelationMapper.class);

    //客户相关的两个表
    private CustomerMapper customerMapper = SqlSessionUtil.getSqlSession().getMapper(CustomerMapper.class);
    private CustomerRemarkMapper customerRemarkMapper = SqlSessionUtil.getSqlSession().getMapper(CustomerRemarkMapper.class);

    //联系人相关的三个表
    private ContactsMapper contactsMapper = SqlSessionUtil.getSqlSession().getMapper(ContactsMapper.class);
    private ContactsRemarkMapper contactsRemarkMapper = SqlSessionUtil.getSqlSession().getMapper(ContactsRemarkMapper.class);
    private ContactsActivityRelationMapper contactsActivityRelationMapper = SqlSessionUtil.getSqlSession().getMapper(ContactsActivityRelationMapper.class);

    //交易人相关的两个表
    private TranMapper tranMapper = SqlSessionUtil.getSqlSession().getMapper(TranMapper.class);
    private TranHistoryMapper tranHistoryMapper = SqlSessionUtil.getSqlSession().getMapper(TranHistoryMapper.class);

    @Override
    public boolean convert(String clueId, Tran t, String createBy) {

        String createTime = DateTimeUtil.getSysTime();

        boolean flag = true;

        // (1)通过线索id获取线索对象（线索对象当中封装了线索的信息）
        Clue c = clueMapper.getId(clueId);
        //(2) 通过线索对象提取客户信息，当该客户不存在的时候，新建客户（根据公司的名称精确匹配，判断该客户是否存在！）
        String company = c.getCompany();
        Customer cus = customerMapper.getCustomerByName(company);
        //如果cousomer为null,说明以前没有这个客户需要新建一个
        if (cus == null) {
            cus = new Customer();
            cus.setId(UUIDUtil.getUUID());
            cus.setAddress(c.getAddress());
            cus.setWebsite(c.getWebsite());
            cus.setPhone(c.getPhone());
            cus.setOwner(c.getOwner());
            cus.setNextContactTime(c.getNextContactTime());
            cus.setName(company);
            cus.setDescription(c.getDescription());
            cus.setCreateBy(createBy);
            cus.setCreateTime(createTime);
            cus.setContactSummary(c.getContactSummary());
            //添加客户
            int count1 = customerMapper.save(cus);
            if (count1 != 1) {
                flag = false;
            }
        }
        //(3) 通过线索对象提取联系人信息，保存联系人
        Contacts con = new Contacts();
        con.setId(UUIDUtil.getUUID());
        con.setSource(c.getSource());
        con.setOwner(c.getOwner());
        con.setNextContactTime(c.getNextContactTime());
        con.setMphone(c.getMphone());
        con.setJob(c.getJob());
        con.setFullname(c.getFullname());
        con.setEmail(c.getEmail());
        con.setDescription(c.getDescription());
//        con.setCustomerId(cus.getId());
        con.setCreateBy(createBy);
        con.setCreateTime(createTime);
        con.setContactSummary(c.getContactSummary());
        con.setAppellation(c.getAppellation());
        con.setAddress(c.getAddress());
        //添加联系人
        int count2 = contactsMapper.save(con);
        if (count2 != 1) {
            flag = false;
        }
        //(4) 线索备注转换到客户备注以及联系人备注
        List<ClueRemark> clueRemarkList = clueRemarkMapper.getListByClueId(clueId);
        if(clueRemarkList != null) {
            //取出每一条线索的备注
            for (ClueRemark clueRemark : clueRemarkList) {
                //取出主要信息(备注信息)
                String noteContent = clueRemark.getNoteContent();
                //创建客户备注对象,添加客户备注
                CustomerRemark customerRemark = new CustomerRemark();
                customerRemark.setId(UUIDUtil.getUUID());
                customerRemark.setNoteContent(noteContent);
                customerRemark.setCreateBy(createBy);
                customerRemark.setCreateTime(createTime);
                customerRemark.setEditFlag("0");
                customerRemark.setCustomerId(cus.getId());
                int count3 = customerRemarkMapper.save(customerRemark);
                if (count3 != 1) {
                    flag = false;
                }
                //创建联系人备注对象,添加联系人备注
                ContactsRemark contactsRemark = new ContactsRemark();
                contactsRemark.setId(UUIDUtil.getUUID());
                contactsRemark.setCreateTime(createTime);
                contactsRemark.setCreateBy(createBy);
                contactsRemark.setNoteContent(noteContent);
                contactsRemark.setEditFlag("0");
                contactsRemark.setContactsId(con.getId());
                int count4 = contactsRemarkMapper.save(contactsRemark);
                if (count4 != 1) {
                    flag = false;
                }
            }
        }
        //(5) “线索和市场活动”的关系转换到“联系人和市场活动”的关系
        List<ClueActivityRelation> clueActivityRelationList = clueActivityRelationMapper.getListByClueId(clueId);
        for (ClueActivityRelation clueActivityRelation : clueActivityRelationList) {
            //创建 联系人与市场活动的关联关系对象 让第三步生成的联系人与市场活动做关联
            ContactsActivityRelation contactsActivityRelation = new ContactsActivityRelation();
            contactsActivityRelation.setId(UUIDUtil.getUUID());
            contactsActivityRelation.setActivityId(clueActivityRelation.getActivityId());
            contactsActivityRelation.setContactsId(con.getId());
            int count5 = contactsActivityRelationMapper.save(contactsActivityRelation);
            if (count5 != 1) {
                flag = false;
            }
        }
        //(6) 如果有创建交易需求，创建一条交易
        if (t != null) {
            t.setSource(c.getSource());
            t.setOwner(c.getOwner());
            t.setNextContactTime(c.getNextContactTime());
            t.setDescription(c.getDescription());
            t.setCustomerId(cus.getId());
            t.setContactSummary(c.getContactSummary());
            t.setType("新业务");
            t.setContactsId(con.getId());
            //添加交易
            int count6 = tranMapper.save(t);
            if (count6 != 1) {
                flag = false;
            }
            //(7)如果创建了交易，则创建一条该交易下的交易历史
            TranHistory th = new TranHistory();
            th.setId(UUIDUtil.getUUID());
            th.setCreateBy(createBy);
            th.setCreateTime(createTime);
            th.setExpectedDate(t.getExpectedDate());
            th.setMoney(t.getMoney());
            th.setStage(t.getStage());
            th.setTranId(t.getId());
            //添加交易历史
            int count7 = tranHistoryMapper.save(th);
            if (count7 != 1) {
                flag = false;
            }
        }
        //(8)删除线索备注
        for (ClueRemark clueRemark : clueRemarkList) {

            int count8 = clueRemarkMapper.delete(clueRemark);
            if (count8 != 1) {
                flag = false;
            }

        }

        //(9) 删除线索和市场活动的关系
        for (ClueActivityRelation clueActivityRelation : clueActivityRelationList) {

            int count9 = clueActivityRelationMapper.delete(clueActivityRelation);
            if (count9 != 1) {

                flag = false;
            }
        }
        //(10) 删除线索
        int count10 = clueMapper.delete(clueId);
        if (count10 != 1) {
            flag = false;
        }
        return flag;
    }

    @Override
    public Map<String, Object> getUserListAndclue(String id) {
        List<User> uList = userMapper.getUserList();
        Clue a = clueMapper.getId(id);
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("uList", uList);
        map.put("a", a);
        return map;
    }

    @Override
    public boolean update(Clue a) {
        boolean flag = true;

        int count = clueMapper.update(a);

        if (count != 1) {
            flag = false;
        }
        return flag;
    }

    @Override
    public List<ClueRemark> getRemarkListByAid(String clueId) {
        List<ClueRemark> arList = clueRemarkMapper.getRemarkListByAid(clueId);
        return arList;
    }

    @Override
    public Boolean saveRemark(ClueRemark ar) {
        boolean flag = true;
        int count = clueRemarkMapper.saveRemark(ar);
        if (count != 1) {
            flag = false;
        }
        return flag;
    }

    @Override
    public boolean save(Clue c) {
        boolean flag = true;
        int count = clueMapper.save(c);
        if (count != 1) {
            flag = false;
        }
        return flag;
    }

    @Override
    public PaginationVO<Clue> pageList(Map<String, Object> map) {
        //取得total
        int total = clueMapper.getTotalByCondition(map);

        //取得dataList
        List<Clue> dataList = clueMapper.getclueByCondition(map);
        PaginationVO<Clue> vo = new PaginationVO<>();
        vo.setTotal(total);
        vo.setDataList(dataList);
        return vo;
    }

    @Override
    public Clue detail(String id) {
        Clue c = clueMapper.detail(id);
        return c;
    }

    @Override
    public boolean unbund(String id) {
        boolean flag = true;
        int count = clueActivityRelationMapper.unbund(id);
        if (count != 1) {
            flag = false;
        }
        return flag;
    }

    @Override
    public boolean bund(String cid, String[] aids) {

        boolean flag = true;

        for (String aid : aids) {
            //取得每一个aid和cid作关联
            ClueActivityRelation car = new ClueActivityRelation();
            car.setId(UUIDUtil.getUUID());
            car.setClueId(cid);
            car.setActivityId(aid);
            //添加关联关系表中的信息
            int count = clueActivityRelationMapper.bund(car);
            if (count != 1) {
                flag = false;
            }
        }
        return flag;
    }

    @Override
    public boolean delete(String[] ids) {
        boolean flag = true;
        // 查询出需要删除的备注数量
        int count1 = clueRemarkMapper.getCountByAids(ids);
        //删除备注,返回收到的影响行数
        int count2 = clueRemarkMapper.deleteByAids(ids);
        if (count1 != count2) {
            flag = false;
        }
        int count4 = clueActivityRelationMapper.deletebatch(ids);
        if (count4 != 1) {
            flag = false;
        }
        int count3 = clueMapper.deletebatch(ids);
        if (count3 != ids.length) {
            flag = false;
        }
        return flag;
    }
}
