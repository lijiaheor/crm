package com.ljh.crm.workbench.service;

import com.ljh.crm.vo.PaginationVO;
import com.ljh.crm.workbench.pojo.Contacts;
import com.ljh.crm.workbench.pojo.ContactsRemark;

import java.util.List;
import java.util.Map;

public interface ContactsService {
    //    为查询按钮添加分页查询和模糊查询并且给列表排序
    PaginationVO<Contacts> pageList(Map<String, Object> map);

    //    通过名称跳转到详细页面
    Contacts detail(String id);

    //对联系人备注进行添加
    List<ContactsRemark> getRemarkListByCid(String contactsId);

    //    对联系人备注信息进行添加
    Boolean saveRemark(ContactsRemark ar);

    //    进行联系人的添加操作
    boolean save(Contacts c);

    //    为修改按钮绑定事件，打开修改操作的模态窗口
    Map<String, Object> getUserListAndCustomer(String id);

    //    进行联系人信息的修改
    boolean update(Contacts a);

    //    进行联系人的删除操作
    boolean delete(String[] ids);
}
