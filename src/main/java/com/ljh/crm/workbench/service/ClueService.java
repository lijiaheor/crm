package com.ljh.crm.workbench.service;

import com.ljh.crm.vo.PaginationVO;
import com.ljh.crm.workbench.pojo.Clue;
import com.ljh.crm.workbench.pojo.ClueRemark;
import com.ljh.crm.workbench.pojo.Tran;

import java.util.List;
import java.util.Map;

public interface ClueService {
    //为添加模态窗口中的保存按钮添加事件,执行线索的添加操作
    boolean save(Clue c);

    //为查询按钮添加分页查询和模糊查询并且给列表排序
    PaginationVO<Clue> pageList(Map<String, Object> map);


    //    通过名称跳转到详细页面
    Clue detail(String id);

    //    进行线索详细页面的解除关联操作
    boolean unbund(String id);

    //    进行关联市场活动中的模态窗口中的与线索绑定关联的操作
    boolean bund(String cid, String[] aids);

    //    执行删除线索的操作
    boolean delete(String[] ids);

    //    为转换页面中的转换按钮添加事件
    boolean convert(String clueId, Tran t, String createBy);

    //    为打开线索的修改模态窗口,来获取信息
    Map<String, Object> getUserListAndclue(String id);

    //    为修改按钮绑定事件,执行线索的修改
    boolean update(Clue a);

    //    对备注信息信息的列表进行展示
    List<ClueRemark> getRemarkListByAid(String clueId);

    //    添加线索的备注信息
    Boolean saveRemark(ClueRemark ar);
}
