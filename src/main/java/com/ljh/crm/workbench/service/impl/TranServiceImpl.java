package com.ljh.crm.workbench.service.impl;

import com.ljh.crm.utils.SqlSessionUtil;
import com.ljh.crm.vo.PaginationVO;
import com.ljh.crm.workbench.mapper.TranMapper;
import com.ljh.crm.workbench.pojo.Contacts;
import com.ljh.crm.workbench.pojo.Customer;
import com.ljh.crm.workbench.pojo.Tran;
import com.ljh.crm.workbench.pojo.TranHistory;
import com.ljh.crm.workbench.service.TranService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TranServiceImpl implements TranService {
    private TranMapper tranMapper = SqlSessionUtil.getSqlSession().getMapper(TranMapper.class);

    @Override
    public Map<String, Object> getCharts() {

        //取得total
        int total = tranMapper.getTotal();
        //取得dataList
        List<Map<String, Object>> dataList = tranMapper.getCharts();
        //放入到Map集合中
        Map<String, Object> map = new HashMap<>();
        map.put("total", total);
        map.put("dataList", dataList);
        return map;
    }

    @Override
    public PaginationVO<Tran> pageList(Map<String, Object> map) {
        //取得total
        int total = tranMapper.getTotalByCondition(map);
        //取得dataList
        List<Tran> dataList = tranMapper.gettranByCondition(map);
        //创建一个vo对象,将total和dataList封装到vo中
        PaginationVO<Tran> vo = new PaginationVO<Tran>();

        vo.setTotal(total);
        vo.setDataList(dataList);

        return vo;
    }

    @Override
    public Tran detail(String id) {
        Tran c = tranMapper.detail(id);
        return c;
    }

    @Override
    public boolean delete(String[] ids) {
        boolean flag = true;
        //删除交易信息
        int count3 = tranMapper.deletebatch(ids);
        if (count3 != ids.length) {
            flag = false;
        }
        return flag;
    }

    @Override
    public List<TranHistory> getHistoryListByTranId(String tranId) {
        List<TranHistory> thList = tranMapper.getHistoryListByTranId(tranId);

        return thList;
    }
}
