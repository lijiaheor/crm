package com.ljh.crm.exception;

public class LoginException extends Exception{
    public LoginException(String msg){
        super(msg);
    }
}
