<%@ page contentType="text/html;charset=UTF-8" language="java" %><%--定义字符编码防止字符乱码--%>
<%
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath() + "/";
%>


<html>
<head>
    <base href="<%=basePath%>">
    <title>mytitle</title>
</head>
<body>
<%--使用数据字典时要使用--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--以上内容是将html文件改为jsp文件必须要用到的--%>
<%--
    在前端中给起名字时就是 id="" 中的内容
    查询:文本框用search- 按钮用searchBtn
    修改:文本框用edit- 按钮用editBtn
    删除:文本框用delete- 按钮用deleteBtn
    添加:文本框用add- 按钮用addBtn
--%>
<%--Ajax中：
1.get 获取服务器中的数据。适用于查询、获取资源或是展示数据的场景，(查询) select
2.post 请求适用于新增、修改或是提交数据的场景。(插入) insert
3.put 请求适用于修改已有资源的场景。(修改) update
4.deletebatch 请求用于删除服务器上的资源，不需要传输数据。(删除)deletebatch
--%>
$.ajax({
url:"",
data:{

},
type: "",
dataType:"json",
success : function (data) {

}
})

//2133333333333333
<%--//创建时间为当前系统时间--%>
String createTime = DateTimeUtil.getSysTime();
<%--//创建人:当前登录用户--%>
String createBy = ((User) request.getSession().getAttribute("user")).getName();


<%--日历控件--%>
$(".time").datetimepicker({
minView: "month",
language: 'zh-CN',
format: 'yyyy-mm-dd',
autoclose: true,
todayBtn: true,
pickerPosition: "bottom-left"
});

<%--分页插件--%>
$("#activityPage").bs_pagination({
currentPage: pageNo, // 页码
rowsPerPage: pageSize, // 每页显示的记录条数
maxRowsPerPage: 20, // 每页最多显示的记录条数
totalPages: totalPages, // 总页数
totalRows: data.total, // 总记录条数

visiblePageLinks: 3, // 显示几个卡片

showGoToPage: true,
showRowsPerPage: true,
showRowsInfo: true,
showRowsDefaultInfo: true,

onChangePage : function(event, data){
pageList(data.currentPage , data.rowsPerPage);
}
});

<%--jsp中的九大内置对象--%>
pageContext page
request response
session
application config
out
exception
</body>
</html>
