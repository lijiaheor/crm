<%--
  Created by IntelliJ IDEA.
  User: 归尘终不见她
  Date: 2023/10/30
  Time: 17:37
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
  String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath() + "/";
  /**
   * 需求:根据交易表的不同的阶段的数量进行统计,最终形成一个漏斗图(倒三角)
   *
   */
%>
<html>
<head>
  <title>ECharts</title>
  <base href="<%=basePath%>">
  <script src="ECharts\echarts.min.js"></script>
  <script src="jquery\jquery-1.11.1-min.js"></script>
  <script>
    $(function () {
      //页面加载完毕后绘制统计图表
      getCharts();
    })

    function getCharts() {
      $.ajax({
        url:"workbench/activity/getCharts.do",
        type: "get",
        dataType:"json",
        success : function (data) {
          // 基于准备好的dom，初始化echarts实例
          var myChart = echarts.init(document.getElementById('main'));

          // 指定图表的配置项和数据
          var option = {
            title: {
              text: '市场活动柱状图图',
              subtext:'统计市场活动名称及其成本的柱状图'
            },
            xAxis: {
              type: 'category',
              data: data.nameList
            },
            yAxis: {
              type: 'value'
            },
            series: [
              {
                data: data.costList,
                type: 'bar'
              }
            ]
          };

          // 使用刚指定的配置项和数据显示图表。
          myChart.setOption(option);
        }
      })

    }
  </script>
</head>
<body>
<!-- 为 ECharts 准备一个定义了宽高的 DOM -->
<div id="main" style="width: 600px;height:400px;"></div>

</body>
</html>
