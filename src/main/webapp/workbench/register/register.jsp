<%@ page contentType="text/html;charset=UTF-8" language="java" %><%--定义字符编码防止字符乱码--%>
<%
String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath() + "/";
%>
<!DOCTYPE html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <base href="<%=basePath%>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="workbench/register/styles.css">
    <title>注册页面</title>
</head>
<body>
<div class="container">
    <h1>欢迎注册</h1>
    <form>
        <label for="username">用户名：</label>
        <input type="text" id="username" name="username" required>

        <label for="name">姓名：</label>
        <input type="text" id="name" name="name" required>

        <label for="password">密码：</label>
        <input type="password" id="password" name="password" required>

        <label for="confirm-password">确认密码：</label>
        <input type="password" id="confirm-password" name="confirm-password" required>

        <button type="submit">注册</button>
    </form>
</div>
</body>
</html>