package com.ljh.crm.test1;


import com.ljh.crm.utils.DateTimeUtil;
import com.ljh.crm.utils.MD5Util;
import org.junit.Test;

public class Test1 {


    /**
     * 判断失效时间
     */
    @Test
    public void expireTime() {
        String expireTime = "2019-10-10 10:10:10";
        String currentTime = DateTimeUtil.getSysTime();
        int count = expireTime.compareTo(currentTime);
        System.out.println(count);
    }

    /**
     * 判断锁定状态
     */
    @Test
    public void lockState() {
        String lockState = "0";
        if ("0".equals(lockState)) {
            System.out.println("账号已锁定");
        }
    }

    /**
     * 判断IP地址
     */
    @Test
    public void allowIps() {
        String ip = "192.168.1.1";
        String allowIps = "192.168.1.1,192.168.1.2";
        if (allowIps.contains(ip)) {
            System.out.println("有效的ip地址");
        } else {
            System.out.println("ip地址受限,请联系管理员");
        }
    }

    /**
     * 判断密码
     */
    @Test
    public void pwd() {
        String pwd = "y65y65yy65y65";
        pwd = MD5Util.getMD5(pwd);
        System.out.println(pwd);
    }


}
